
[[ -f ~/.profile ]] && source ~/.profile

source ${HOME}/.zgen/zgen.zsh

if ! zgen saved; then
    zgen oh-my-zsh

    zgen oh-my-zsh plugins/vi-mode
    zgen oh-my-zsh plugins/last-working-dir

    # fish-style plugins
    zgen load zsh-users/zsh-history-substring-search

    # Completions
    zgen oh-my-zsh plugins/git
    zgen oh-my-zsh plugins/gitfast

    [[ -f ~/.dotfiles/local/zsh-plug ]] && source ~/.dotfiles/local/zsh-plug

    zgen apply
    zgen save
fi

[[ -f /usr/share/doc/pkgfile/command-not-found.zsh ]] &&
    source /usr/share/doc/pkgfile/command-not-found.zsh


# Editing
bindkey -M viins 'jk' vi-cmd-mode

# bind UP and DOWN arrow keys
zmodload zsh/terminfo

if (( $+commands[mcfly] )); then
    eval "$(mcfly init zsh)"
    bindkey "$terminfo[kcud1]" mcfly-history-widget
    bindkey "$terminfo[kcuu1]" mcfly-history-widget
else
    bindkey "$terminfo[kcud1]" history-substring-search-down
    bindkey "$terminfo[kcuu1]" history-substring-search-up
fi

eval "$(dircolors $HOME/.dotfiles/dircolors)"
source ~/.dotfiles/kilogram.zsh-theme
# Colorize completions using default `ls` colors.
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"

# Configure python environment
export WORKON_HOME=$HOME/.venvs
export PROJECT_HOME=$HOME/src/
export PIP_VIRTUALENV_BASE=$WORKON_HOME
export PIP_RESPECT_VIRTUALENV=true

source ~/.dotfiles/aliases

[[ -f ~/.dotfiles/local/zshrc ]] && source ~/.dotfiles/local/zshrc || true
