
short_dir() { echo "%~" }
PROMPT='%F{red}%m%f [%*] %F{214}%(!.#.»)%f '
RPS1='%F{red}$(vi_mode_prompt_info)%f $(virtualenv_prompt_info)$(git_prompt_info)%F{214}$(short_dir)%f %(?..%F{red}%? :(%f)'

ZSH_THEME_GIT_PROMPT_PREFIX="%{$FG[226]%}("
ZSH_THEME_GIT_PROMPT_SUFFIX=") ::%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_CLEAN=""
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}*%{$FG[226]%}"

ZSH_THEME_VIRTUAL_ENV_PROMPT_PREFIX="%F{075}<"
ZSH_THEME_VIRTUAL_ENV_PROMPT_SUFFIX="> %f"

setopt TRANSIENT_RPROMPT

prompt-time-accept-line () {
  zle reset-prompt
  zle accept-line
}

zle -N prompt-time-accept-line
bindkey "^M" prompt-time-accept-line
