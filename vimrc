set nocompatible
if has('nvim')
    lua package.path = package.path .. ";/u/kilogram/.dotfiles/local/?.lua"
    set runtimepath+=~/.vim,~/.vim/after
    set packpath+=~/.vim
endif
let mapleader="\<Space>"

function! EnsureDirExists (dir)
    if !isdirectory(a:dir) && exists("*mkdir")
        call mkdir(a:dir, 'p')
    endif
endfunction

call EnsureDirExists('/tmp/vim')
set directory=/tmp/vim//
let g:netrw_silent = 1
set modeline
set gdefault
" encoding settings {{{
if has("multi_byte")
    if &termencoding == ""
        let &termencoding = &encoding
    endif
    set fileencodings=ucs-bom,utf-8,latin1
endif
" }}}
" Remaps {{{
inoremap jk <esc>
nnoremap <CR> :update<CR>
" <CR> in quickfix window jumps to location under cursor
autocmd BufReadPost quickfix nnoremap <buffer> <CR> <CR>
set cedit=\<C-A>
augroup commandlinewindow
    autocmd!
    autocmd CmdwinEnter * nnoremap <buffer> <CR> <CR>
augroup END
nnoremap <leader><S-r> :source ~/.vimrc<CR>
" Tab toggles folds
nnoremap <silent> <Tab> @=(foldlevel('.')?'za':"\<Tab>")<CR>
vnoremap <Tab> zf
nnoremap <leader>. <C-^>
" nobuflisted the quickfix buffer
augroup QFix
    autocmd!
    autocmd FileType qf setlocal nobuflisted
augroup END
" Movement keys
nnoremap k gk
nnoremap j gj
nnoremap <up> 3<C-Y>
inoremap <up> 3<C-Y>
nnoremap <down> 3<C-E>
inoremap <down> 3<C-E>
nnoremap <left> <nop>
nnoremap <right> <nop>

cabbr <expr> %% expand('%:p:h')
cmap w!! w !sudo tee >/dev/null %<Return>L<Return>
" }}}
if executable('pt')
    set grepprg=pt\ --nogroup\ --nocolor
elseif executable('ag')
    set grepprg=ag\ --nogroup\ --nocolor
endif
" Set up plugins {{{
call plug#begin('~/.vim/plugged')

let g:git_branch_status_ignore_remotes = 1

" Startup {{{
    Plug 'mhinz/vim-startify'
    Plug 'vim-scripts/restore_view.vim'
    set viewoptions=cursor,folds
    Plug 'tmux-plugins/vim-tmux-focus-events'
    Plug 'rdavison/Libertine'
" }}}
" Source control {{{
    Plug 'tpope/vim-fugitive'
        noremap <leader>g :Gstatus<CR>

    Plug 'mhinz/vim-signify'
        let g:signify_sign_add = '•'
        let g:signify_sign_change = '•'
        let g:signify_sign_delete = '•'
        let g:signify_sign_delete_first_line = '•'
        highlight SignifySignAdd ctermbg=30
        highlight SignifySignChange ctermbg=178
        highlight SignifySignDelete ctermbg=166
        highlight SignifySignDeleteFirstLine ctermfg=178 ctermbg=166
" }}}
" iMproved editing {{{
    Plug 'wellle/targets.vim'
    Plug 'michaeljsmith/vim-indent-object'
    Plug 'jeetsukumaran/vim-indentwise'

    Plug 'tpope/vim-abolish'
    Plug 'tpope/vim-commentary'
    Plug 'tpope/vim-surround'
        let g:surround_no_insert_mappings=1
    Plug 'tpope/vim-repeat'
" }}}
" Tools and Utilities {{{
    Plug 'mhinz/vim-sayonara', { 'on': 'Sayonara' }
        nmap <leader>q :Sayonara<CR>
        nmap <leader>d :Sayonara!<CR>

    Plug 'mbbill/undotree', { 'on': 'UndotreeToggle' }
        noremap <leader>u :UndotreeToggle<CR>

    Plug 'vim-scripts/nextval'
        nmap <silent> <C-a> <Plug>nextvalInc
        nmap <silent> <C-x> <Plug>nextvalDec

    Plug 'lilydjwg/colorizer'
    Plug 'tpope/vim-sleuth'

    Plug 'christoomey/vim-tmux-navigator'
        let g:tmux_navigator_no_mappings = 1
        nnoremap <silent> <M-h> :TmuxNavigateLeft<CR>
        nnoremap <silent> <M-j> :TmuxNavigateDown<CR>
        nnoremap <silent> <M-k> :TmuxNavigateUp<CR>
        nnoremap <silent> <M-l> :TmuxNavigateRight<CR>

    Plug 'junegunn/vim-peekaboo'
" }}}
" Code navigation {{{
    Plug 'ctrlpvim/ctrlp.vim'
        let g:ctrlp_map = '<leader>,'
        let g:ctrlp_cmd = 'CtrlPBuffer'
        let g:ctrlp_cache_dir = $HOME . '/.cache/ctrlp'
        if executable('ag')
            let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
        endif
" }}}
" Syntax {{{
    Plug 'tmux-plugins/vim-tmux', { 'for': 'tmux' }

    Plug 'rust-lang/rust.vim', { 'for': 'rust' }
        let g:rustfmt_autosave = 1

    Plug 'dense-analysis/ale'
        let g:ale_fix_on_save = 1
        autocmd FileType diff,gitcommit let b:ale_fix_on_save = 0

" }}}
" Local plugins {{{
let $LOCAL_PLUGINS=expand("~/.dotfiles/local/vim-plug")
if filereadable($LOCAL_PLUGINS)
    source $LOCAL_PLUGINS
endif
" }}}
call plug#end()
" }}}
" Setting up views {{{
set scrolloff=3
set number
set relativenumber
set shortmess=at
set foldcolumn=1
set list
nnoremap <leader>s :set list!<CR>
set listchars=tab:»\ ,eol:¬
set backspace=indent,eol,start
call EnsureDirExists(expand('~/.cache/vim/view'))
set viewdir=~/.cache/vim/view
call EnsureDirExists(expand('~/.cache/vim/undo'))
set undodir=~/.cache/vim/undo
set undofile
set undolevels=1000 " Max changes
set undoreload=10000 " Max number to save on buffer reload
augroup qf
    autocmd!
    autocmd FileType qf set nobuflisted
augroup END
" Set up focus mode {{{
let s:focus_mode = 0
function! s:toggle_focus()
    if s:focus_mode == 0
        let s:focus_mode = 1
        set nolist
        set norelativenumber
        set nonumber
        set noruler
        set signcolumn=no
        set foldcolumn=0
        set noshowcmd
        set noshowmode
        set laststatus=0
    else
        let s:focus_mode = 0
        set laststatus=2
        set showmode
        set showcmd
        set foldcolumn=1
        set signcolumn=auto
        set ruler
        set number
        set relativenumber
        set list
    endif
endfunction
command! Focus :call s:toggle_focus()
" }}}
" }}}
" Filetype and syntax and search highlighting. {{{
syntax on
filetype off
filetype plugin indent on
setlocal foldmethod=manual
let @/ = ""
set hlsearch
set incsearch
nnoremap <leader>' :let @/='\<<C-R>=expand("<cword>")<CR>\>'<CR>:set hls<CR>
nnoremap <leader>; :set hlsearch!<CR>
nnoremap / :set hlsearch<CR>/
set ignorecase
set smartcase
" }}}
" Setting up statusline {{{
let g:git_branch_status_text=""
set ls=2
set statusline=%.50F            "Truncated file path
set statusline+=%h%m%r          "help, modified, and read-only
set statusline+=%y              "filetype
set statusline+=%#warningmsg#
set statusline+=%{exists('g:loaded_syntastic_plugin')?SyntasticStatuslineFlag():''}
set statusline+=%*
set statusline+=%=              "left/right separator
set statusline+=\ [%{&fo}]
set statusline+=<%{exists('g:loaded_fugitive')?fugitive#statusline():''}>
set statusline+=%c,%l/%L\ %P    "cursor and file position
" }}}
" Set up general autocommands {{{
augroup all_autocmds
    autocmd!
    autocmd BufEnter silent! RainbowParentheses
augroup END
" }}}
" Set up python autocommands {{{
augroup python_autocmds
    autocmd!
    " highlight characters past column 80
    autocmd FileType python match Excess /\%>80v.\+\%<101v/
    autocmd FileType python 2match Excess2 /\%100v.*/
    autocmd FileType python set nowrap
    autocmd FileType python set commentstring=#\ %s
augroup END
" }}}
" Set up C autocommands {{{
augroup c_autocmds
    autocmd!
    " highlight characters past column 80
    autocmd FileType c,cpp match Excess2 /\%80v.*/
    autocmd FileType c,cpp set nowrap
    autocmd FileType c,cpp set commentstring=//\ %s
augroup END
" }}}
" Set up lisp autocommands {{{
augroup lisp_autocmds
    " highlight characters past column 80
    autocmd FileType lisp,clojure,scheme match Excess2 /\%80v.*/
    autocmd FileType lisp,clojure,scheme set nowrap
    autocmd FileType lisp,clojure,scheme set commentstring=;;\ %s
    autocmd FileType lisp,clojure,scheme set lisp
augroup END
" }}}
" Set up vim autocommands {{{
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
    " Auto refresh .vimrc upon change.
    autocmd BufWritePost .vimrc source %
augroup END
" }}}
" Set up tex autocommands {{{
augroup filetype_tex
    autocmd!
    autocmd FileType tex set textwidth=81
    autocmd FileType tex Latexmk
augroup END
" }}}
" Set up text autocommands {{{
augroup filetype_text
    autocmd!
    autocmd FileType markdown,yaml,text set textwidth=81
    autocmd FileType markdown,yaml,text match Excess /\%>80v.\+\%<101v/
augroup END
" }}}
" Set up dotfiles autocommands {{{
augroup dotfiles
    autocmd!
    autocmd BufWritePost $HOME/.dotfiles/* :silent !$HOME/.dotfiles/update
augroup END
" }}}
" Coloring {{{
let g:libertine_Twilight = 1
silent! color libertine
highlight StatusLine ctermbg=214 ctermfg=88
highlight StatusLineNC ctermbg=196 ctermfg=0
highlight Excess ctermbg=237 guibg=Black
highlight Excess2 ctermbg=241 guibg=Black
highlight MatchParen cterm=NONE,bold ctermbg=242
highlight htmlBold gui=bold guifg=#661188 ctermfg=54 cterm=bold
highlight htmlItalic gui=italic guifg=#664488 ctermfg=60 cterm=italic
" }}}
let $LOCAL_FILE=expand("~/.dotfiles/local/vimrc")
if filereadable($LOCAL_FILE)
    source $LOCAL_FILE
endif
