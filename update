#!/bin/bash

set -o nounset
set -o errexit
set -o pipefail

THIS_FILE=$( basename "${BASH_SOURCE[0]}" )
SRC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DEST="${1:-$HOME}"
CONFIG="${XDG_CONFIG_HOME:-$DEST/.config}"

mkdir -p ${DEST} >/dev/null 2>&1
mkdir -p ${CONFIG} >/dev/null 2>&1

echo -n "Copying dotfiles... "
for f in zshrc bashrc tmux.conf vimrc gitconfig; do
  cp -f ${SRC}/${f} ${DEST}/.${f}
  chmod a-w ${DEST}/.${f}
done
[[ $? -eq 0 ]] && echo "done"

echo -n "Installing zgen... "
if ! [[ -d ${DEST}/.zgen ]]; then
  git clone https://github.com/tarjoilija/zgen ${DEST}/.zgen >/dev/null 2>&1
fi
[[ $? -eq 0 ]] && echo "done"

echo -n "Setting up neovim... "
if ! [[ -d ${CONFIG}/nvim ]]; then
  mkdir -p ${CONFIG}/nvim >/dev/null 2>&1
  ln -s ${HOME}/.vim ${CONFIG}/nvim && \
    ln -s ${DEST}/.vimrc ${CONFIG}/nvim/init.vim
fi
[[ $? -eq 0 ]] && echo "done"

echo -n "Installing vim-plug... "
if ! [[ -d ${DEST}/.vim/autoload/plug.vim ]]; then
  curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim >/dev/null 2>&1
fi
[[ $? -eq 0 ]] && echo "done"

echo -n "Updating ssh config... "
mkdir -p ${DEST}/.ssh >/dev/null 2>&1
chmod a+w ${DEST}/.ssh/config >/dev/null 2>&1 || true
cat \
  <( find -L ${SRC}/ssh.d -type f 2>/dev/null ) \
  <( find -L ${SRC}/local/ssh.d -type f 2>/dev/null ) \
  | while read f; do echo "$( basename ${f} )?${f}"; done \
  | sort | awk -F"?" '{print $2}' | xargs cat > ${DEST}/.ssh/config && \
  chmod a-w ${DEST}/.ssh/config
[[ $? -eq 0 ]] && echo "done"
